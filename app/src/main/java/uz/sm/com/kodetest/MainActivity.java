package uz.sm.com.kodetest;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import uz.sm.com.kodetest.entities.City;
import uz.sm.com.kodetest.entities.SimpleItem;
import uz.sm.com.kodetest.fragments.BaseFragment;
import uz.sm.com.kodetest.fragments.CitiesPage;
import uz.sm.com.kodetest.fragments.CountriesPage;

public class MainActivity extends BaseActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private int mCurrItemPos = 0;
    private String mCurrItemTag = null;

    public ArrayList<SimpleItem> countries;
    public ArrayList<City> cities;
    public int selectedCountryPos = 0;

    @Bind(R.id.progress_bar_block)
    public View mProgressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        openFragPage(0, false, null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        TODO handling back press
    }

    public void openFragPage(int pageNum, boolean needBackStack, Object... args) {
        BaseFragment mCurrFrag = null;
        switch (pageNum) {
            case 0:
                mCurrFrag = new CountriesPage();
                mCurrItemTag = "CountriesPage";
                break;
            case 1:
                mCurrFrag = CitiesPage.newInstance((SimpleItem)args[0]);
                mCurrItemTag = "CitiesPage";
                break;
            default:
                break;
        }
        mCurrItemPos = pageNum;
        if (mCurrFrag != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            if (needBackStack) {
                ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
            }
            ft.replace(R.id.main_container, mCurrFrag, mCurrItemTag);
            if (needBackStack) {
                ft.addToBackStack(mCurrItemTag);
            }
            ft.commit();
        }
    }
}
