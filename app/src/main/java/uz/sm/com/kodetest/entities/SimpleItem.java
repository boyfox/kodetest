package uz.sm.com.kodetest.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mr.boyfox on 22.01.16.
 * StyleMix Mobile LLC. All rights reserved!
 */
public class SimpleItem implements Parcelable {
    public long id;
    public String title;

    public SimpleItem() {}

    public SimpleItem(long id, String title) {
        this.id = id;
        this.title = title;
    }

    @Override
    public String toString() {
        return "SimpleItem{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.title);
    }

    protected SimpleItem(Parcel in) {
        this.id = in.readLong();
        this.title = in.readString();
    }

    public static final Creator<SimpleItem> CREATOR = new Creator<SimpleItem>() {
        public SimpleItem createFromParcel(Parcel source) {
            return new SimpleItem(source);
        }

        public SimpleItem[] newArray(int size) {
            return new SimpleItem[size];
        }
    };
}
