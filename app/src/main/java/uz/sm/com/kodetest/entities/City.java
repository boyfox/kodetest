package uz.sm.com.kodetest.entities;

import android.os.Parcel;

/**
 * Created by mr.boyfox on 22.01.16.
 * StyleMix Mobile LLC. All rights reserved!
 */
public class City extends SimpleItem {
    public int important = 0;

    public City() {}

    public City(long id, String title, int important) {
        super(id, title);
        this.important = important;
    }

    @Override
    public String toString() {
        return "City{" +
                "important=" + important +
                ", id=" + id +
                ", title='" + title + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(this.important);
    }

    protected City(Parcel in) {
        super(in);
        this.important = in.readInt();
    }

    public static final Creator<City> CREATOR = new Creator<City>() {
        public City createFromParcel(Parcel source) {
            return new City(source);
        }

        public City[] newArray(int size) {
            return new City[size];
        }
    };
}
