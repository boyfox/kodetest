package uz.sm.com.kodetest.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.io.ByteArrayInputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.security.auth.x500.X500Principal;

import uz.sm.com.kodetest.KodeApp;

/**
 * Created by mr.boyfox on 22.01.16.
 * StyleMix Mobile LLC. All rights reserved!
 */
public class Utilities {
//    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);
    public static boolean DEBUG = false;
    public static float density = 1f;
    public static Point displaySize = new Point();
//    private static final Hashtable<String, Typeface> typefaceCache = new Hashtable<String, Typeface>();

    static {
        DEBUG = isDebuggable();
        density = KodeApp.mContext.getResources().getDisplayMetrics().density;
        initDspSize();
    }

    private static void initDspSize() {
        try {
            WindowManager manager = (WindowManager) KodeApp.mContext.getSystemService(Context.WINDOW_SERVICE);
            if (manager != null) {
                Display display = manager.getDefaultDisplay();
                DisplayMetrics displayMetrics = new DisplayMetrics();
                if (display != null) {
                    display.getMetrics(displayMetrics);
                    if (Build.VERSION.SDK_INT < 13) {
                        displaySize.set(display.getWidth(), display.getHeight());
                    } else {
                        display.getSize(displaySize);
                    }
                    HLog.e("Utilities", "display size = [" + displaySize.x + ", " + displaySize.y +
                            "] " + displayMetrics.xdpi + "x" + displayMetrics.ydpi);
                }
            }
        } catch (Exception e) {
            HLog.e("Utilities","ExpError", e);
        }
    }

    public static boolean isDebuggable() {
        boolean debuggable = false;
        boolean problemsWithData = false;

        if (KodeApp.mContext == null) return debuggable;

        PackageManager pm = KodeApp.mContext.getPackageManager();
        try {
            ApplicationInfo appinfo = pm.getApplicationInfo(KodeApp.mContext.getPackageName(), 0);
            debuggable = (0 != (appinfo.flags &= ApplicationInfo.FLAG_DEBUGGABLE));
        }
        catch (PackageManager.NameNotFoundException e) {
            problemsWithData = true;
        }

        if (problemsWithData) {
            problemsWithData = false;
            try {
                PackageInfo pinfo = KodeApp.mContext.getPackageManager().getPackageInfo(KodeApp.mContext.getPackageName(),PackageManager.GET_SIGNATURES);
                Signature signatures[] = pinfo.signatures;

                for ( int i = 0; i < signatures.length;i++) {
                    CertificateFactory cf = CertificateFactory.getInstance("X.509");
                    ByteArrayInputStream stream = new ByteArrayInputStream(signatures[i].toByteArray());
                    X509Certificate cert = (X509Certificate) cf.generateCertificate(stream);
                    debuggable = cert.getSubjectX500Principal().equals(new X500Principal("CN=Android Debug,O=Android,C=US"));
                    if (debuggable)
                        break;
                }
            }
            catch (PackageManager.NameNotFoundException e) {
                problemsWithData = true;
            }
            catch (CertificateException e) {
                problemsWithData = true;
            }
        }
        return debuggable;
    }

    public static int dp(float value) {
        if (value == 0) {
            return 0;
        }
        return (int)Math.ceil(density * value);
    }

    public static float sp(int pixel) {
        Resources r = KodeApp.mContext.getResources();
        int sip = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_SP,
                pixel,
                r.getDisplayMetrics()
        );
        return sip;
    }

    public static void showKeyboard(View view) {
        if (view == null) {
            return;
        }
        InputMethodManager inputManager = (InputMethodManager)view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public static boolean isKeyboardShowed(View view) {
        if (view == null) {
            return false;
        }
        InputMethodManager inputManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        return inputManager.isActive(view);
    }

    public static void hideKeyboard(View view) {
        if (view == null) {
            return;
        }
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (!imm.isActive()) {
            return;
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void hideKeyboard(AppCompatActivity act) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) act.getSystemService(Activity.INPUT_METHOD_SERVICE);
            IBinder windowToken = act.getCurrentFocus().getWindowToken();
            if(windowToken != null) inputMethodManager.hideSoftInputFromWindow(windowToken, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public static Typeface getTypeface(String assetPath) {
//        synchronized (typefaceCache) {
//            if (!typefaceCache.containsKey(assetPath)) {
//                try {
//                    Typeface t = Typeface.createFromAsset(KodeApp.mContext.getAssets(), assetPath);
//                    typefaceCache.put(assetPath, t);
//                } catch (Exception e) {
//                    HLog.e("Typefaces", "Could not get typeface '" + assetPath + "' because " + e.getMessage());
//                    return null;
//                }
//            }
//            return typefaceCache.get(assetPath);
//        }
//    }

//    public static Typeface Font_HNLt1gMedium() {
//        return getTypeface("fonts/HelveticaNeueLTW1G-Md.otf");
//    }

//    @SuppressLint("NewApi")
//    public static int newViewId() {
//        if (Build.VERSION.SDK_INT < 17) {
//            for (;;) {
//                final int result = sNextGeneratedId.get();
//                // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
//                int newValue = result + 1;
//                if (newValue > 0x00FFFFFF)
//                    newValue = 1; // Roll over to 1, not 0.
//                if (sNextGeneratedId.compareAndSet(result, newValue)) {
//                    return result;
//                }
//            }
//        } else {
//            return View.generateViewId();
//        }
//    }

    public static boolean isNetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) KodeApp.mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo acNetInfo = connectivityManager.getActiveNetworkInfo();
        return acNetInfo != null && acNetInfo.isAvailable() && acNetInfo.isConnected();
    }
}
