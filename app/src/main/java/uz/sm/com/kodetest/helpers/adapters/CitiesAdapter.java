package uz.sm.com.kodetest.helpers.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import uz.sm.com.kodetest.databinding.CitiesListItemBinding;
import uz.sm.com.kodetest.entities.City;
import uz.sm.com.kodetest.fragments.BaseFragment;

/**
 * Created by mr.boyfox on 22.01.16.
 * StyleMix Mobile LLC. All rights reserved!
 */
public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.ViewHolder> {
    private ArrayList<City> mData;
    private BaseFragment mFrag;
    private LayoutInflater mInflater;

    public CitiesAdapter(BaseFragment fragment, ArrayList<City> mData) {
        this.mFrag = fragment;
        mInflater = LayoutInflater.from(mFrag.getContext());
        this.mData = mData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CitiesListItemBinding clb;

        public ViewHolder(CitiesListItemBinding binder) {
            super(binder.getRoot());
            binder.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    City cItem = mData.get(getAdapterPosition());
                    v.setTag(cItem);
                    mFrag.onClick(v);
                }
            });
            clb = binder;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        CitiesListItemBinding clb = CitiesListItemBinding.inflate(mInflater, viewGroup, false);
        return new ViewHolder(clb);
    }
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        City item = mData.get(position);
        viewHolder.clb.setCity(item);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
