package uz.sm.com.kodetest.httpservices;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by mr.boyfox on 23.01.16.
 * StyleMix Mobile LLC. All rights reserved!
 */
public interface GoogleServices {

    @GET
    Call<JsonObject> getLatLng(@Url String url);
}
