package uz.sm.com.kodetest.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uz.sm.com.kodetest.MainActivity;
import uz.sm.com.kodetest.R;
import uz.sm.com.kodetest.entities.City;
import uz.sm.com.kodetest.entities.SimpleItem;
import uz.sm.com.kodetest.helpers.HLog;
import uz.sm.com.kodetest.helpers.Utilities;
import uz.sm.com.kodetest.helpers.adapters.CountriesAdapter;
import uz.sm.com.kodetest.helpers.customs.DividerItemDecoration;

/**
 * A simple {@link Fragment} subclass.
 */
public class CountriesPage extends BaseFragment {
    private static final String TAG = CountriesPage.class.getSimpleName();

    private MainActivity mAct;
    private CountriesAdapter mAdapter;

    private Call<JsonObject> countriesReq;
    private Call<JsonObject> citiesReq;

    @Bind(R.id.recyclerView)
    RecyclerView countriesRV;

    private SimpleItem selItem;

    public CountriesPage() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_countries_page, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        mAct = (MainActivity) getActivity();
        mAct.getSupportActionBar().setTitle(R.string.countries_txt);

        boolean firstInit = false;
        if (mAct.countries == null) {
            mAct.countries = new ArrayList<>();
            firstInit = true;
        }
        mAdapter = new CountriesAdapter(this, mAct.countries);

        DividerItemDecoration divider = new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL);
        countriesRV.addItemDecoration(divider);
        countriesRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        countriesRV.setAdapter(mAdapter);

        refreshData(firstInit);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                selItem = (SimpleItem) v.getTag();
                prepareToOpenCity();
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mAct.cancelReq(countriesReq, citiesReq);
    }

    private void refreshData(boolean firstInit) {
        if (Utilities.isNetAvailable()) {
            if(firstInit) mAct.mProgressbar.setVisibility(View.VISIBLE);
            countriesReq = mAct.mApp.vkApi.getCountries(1, 1000, "5.44");
            countriesReq.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Response<JsonObject> response) {
                    if (mAct.mProgressbar.isShown()) mAct.mProgressbar.setVisibility(View.GONE);
                    if (response.code() == 200) {
                        JsonObject respJS = response.body().getAsJsonObject("response");
                        handleCountriesJS(respJS);
                    } else {
                        Toast.makeText(getActivity(), "Vk Request Fail", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    if (mAct.mProgressbar.isShown()) mAct.mProgressbar.setVisibility(View.GONE);
                    HLog.e(TAG, "countriesReq fail, error :" + t.getLocalizedMessage());
                    Toast.makeText(getActivity(), "Vk Request Fail", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), R.string.offline_mode_txt, Toast.LENGTH_SHORT).show();
            try {
                InputStream is = mAct.getAssets().open("countries.json");
                int len = is.available();
                byte[] bytes = new byte[len];
                is.read(bytes);
                is.close();
                Type type = new TypeToken<JsonObject>() {}.getType();
                JsonObject respJS = mAct.mApp.gson.fromJson(new String(bytes), type);
                handleCountriesJS(respJS.getAsJsonObject("response"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void handleCountriesJS(JsonObject respJS) {
        if (respJS != null) {
            JsonArray itemsJS = respJS.getAsJsonArray("items");
            Type listType = new TypeToken<ArrayList<SimpleItem>>() {}.getType();
            mAct.countries = mAct.mApp.gson.fromJson(itemsJS, listType);
            mAdapter.setNewData(mAct.countries);
        } else {
            Toast.makeText(getActivity(), "Response is empty", Toast.LENGTH_SHORT).show();
        }
    }

    private void prepareToOpenCity() {
        if (Utilities.isNetAvailable()) {
            mAct.mProgressbar.setVisibility(View.VISIBLE);
            citiesReq = mAct.mApp.vkApi.getCities(selItem.id, 1000, "5.44");
            citiesReq.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Response<JsonObject> response) {
                    mAct.mProgressbar.setVisibility(View.GONE);
                    if (response.code() == 200) {
                        JsonObject respJS = response.body().getAsJsonObject("response");
                        handleCitiesJS(respJS);
                    } else {
                        Toast.makeText(getActivity(), "Vk Request Fail", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    mAct.mProgressbar.setVisibility(View.GONE);
                    HLog.e(TAG, "citiesReq fail, error :" + t.getLocalizedMessage());
                    Toast.makeText(getActivity(), "Vk Request Fail", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), R.string.offline_mode_txt, Toast.LENGTH_SHORT).show();
            if (selItem.id == 1) {
                try {
                    InputStream is = mAct.getAssets().open("cities.json");
                    int len = is.available();
                    byte[] bytes = new byte[len];
                    is.read(bytes);
                    is.close();
                    JsonObject respJS = mAct.mApp.gson.fromJson(new String(bytes), JsonObject.class);
                    handleCitiesJS(respJS.getAsJsonObject("response"));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void handleCitiesJS(JsonObject respJS) {
        if (respJS != null) {
            JsonArray itemsJS = respJS.getAsJsonArray("items");
            Type listType = new TypeToken<ArrayList<City>>() {}.getType();
            mAct.cities = mAct.mApp.gson.fromJson(itemsJS, listType);
            mAct.openFragPage(1, true, selItem);
        } else {
            Toast.makeText(getActivity(), "Response is empty", Toast.LENGTH_SHORT).show();
        }
    }
}
