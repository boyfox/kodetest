package uz.sm.com.kodetest.httpservices;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by mr.boyfox on 22.01.16.
 * StyleMix Mobile LLC. All rights reserved!
 */
public interface VkServices {

    @FormUrlEncoded
    @POST("database.getCountries")
    Call<JsonObject> getCountries(@Field("need_all") int needAll, @Field("count") int count,
                                  @Field("v") String version);

    @FormUrlEncoded
    @POST("database.getCities")
    Call<JsonObject> getCities(@Field("country_id") long countryId, @Field("count") int count,
                                  @Field("v") String version);
}
