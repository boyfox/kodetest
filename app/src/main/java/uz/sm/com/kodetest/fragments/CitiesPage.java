package uz.sm.com.kodetest.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import uz.sm.com.kodetest.MainActivity;
import uz.sm.com.kodetest.MapActivity;
import uz.sm.com.kodetest.R;
import uz.sm.com.kodetest.entities.City;
import uz.sm.com.kodetest.entities.SimpleItem;
import uz.sm.com.kodetest.helpers.adapters.CitiesAdapter;
import uz.sm.com.kodetest.helpers.customs.DividerItemDecoration;

/**
 * A simple {@link Fragment} subclass.
 */
public class CitiesPage extends BaseFragment {
    private static final String TAG = CitiesPage.class.getSimpleName();

    private MainActivity mAct;
    private CitiesAdapter mAdapter;

    @Bind(R.id.recyclerView)
    RecyclerView citiesRV;

    private SimpleItem country;

    public CitiesPage() {
        // Required empty public constructor
    }

    public static CitiesPage newInstance(SimpleItem selCountry) {
        CitiesPage cp = new CitiesPage();
        Bundle data = new Bundle();
        data.putParcelable("country", selCountry);
        cp.setArguments(data);
        return cp;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            country = getArguments().getParcelable("country");
        } else if (savedInstanceState != null) {
            country = savedInstanceState.getParcelable("country");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cities_page, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        mAct = (MainActivity) getActivity();
        mAct.getSupportActionBar().setTitle(R.string.cities_txt);

        if (mAct.cities == null) mAct.cities = new ArrayList<>();
        mAdapter = new CitiesAdapter(this, mAct.cities);

        DividerItemDecoration divider = new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL);
        citiesRV.addItemDecoration(divider);
        citiesRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        citiesRV.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                City item = (City) v.getTag();
                Intent i = new Intent(mAct, MapActivity.class);
                i.putExtra("city", item);
                i.putExtra("country", country);
                mAct.openPage(i);
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("country", country);
        super.onSaveInstanceState(outState);
    }
}
