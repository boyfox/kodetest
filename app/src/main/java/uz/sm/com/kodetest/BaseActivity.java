package uz.sm.com.kodetest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.gson.JsonObject;

import retrofit2.Call;

/**
 * Created by mr.boyfox on 22.01.16.
 * StyleMix Mobile LLC. All rights reserved!
 */
public class BaseActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = BaseActivity.class.getSimpleName();
    public KodeApp mApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApp = (KodeApp) getApplication();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onClick(View v) {}

    public void openPage(Intent actIntent) {
        startActivity(actIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void openPage(Class<?> pageClass) {
        openPage(new Intent(BaseActivity.this, pageClass));
    }

    public void cancelReq(Call<JsonObject>... reqs) {
        for (int i = 0; i < reqs.length; i++) {
            if (reqs[i] != null && (!reqs[i].isCanceled() || !reqs[i].isExecuted())) reqs[i].cancel();
        }
    }
}
