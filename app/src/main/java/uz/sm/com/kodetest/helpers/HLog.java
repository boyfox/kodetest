package uz.sm.com.kodetest.helpers;

import android.util.Log;

/**
 * Created by mr.boyfox on 22.01.16.
 * StyleMix Mobile LLC. All rights reserved!
 */
public class HLog {
    public static void i(String tag, String log) {
        if (Utilities.DEBUG) Log.i(tag, log);
    }
    public static void i(String tag, String log, Exception e) {
        if (Utilities.DEBUG) Log.i(tag, log, e);
    }

    public static void v(String tag, String log) {
        if (Utilities.DEBUG) Log.v(tag, log);
    }
    public static void v(String tag, String log, Exception e) {
        if (Utilities.DEBUG) Log.v(tag, log, e);
    }

    public static void d(String tag, String log) {
        if (Utilities.DEBUG) Log.d(tag, log);
    }
    public static void d(String tag, String log, Exception e) {
        if (Utilities.DEBUG) Log.d(tag, log, e);
    }

    public static void w(String tag, String log) {
        if (Utilities.DEBUG) Log.w(tag, log);
    }
    public static void w(String tag, String log, Exception e) {
        if (Utilities.DEBUG) Log.w(tag, log, e);
    }

    public static void e(String tag, String log) {
        if (Utilities.DEBUG) Log.e(tag, log);
    }
    public static void e(String tag, String log, Exception e) {
        if (Utilities.DEBUG) Log.e(tag, log, e);
    }
}