package uz.sm.com.kodetest;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;

import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import uz.sm.com.kodetest.helpers.KodaConfig;
import uz.sm.com.kodetest.httpservices.GoogleServices;
import uz.sm.com.kodetest.httpservices.VkServices;

/**
 * Created by mr.boyfox on 22.01.16.
 * StyleMix Mobile LLC. All rights reserved!
 */
public class KodeApp extends Application {
    private static final String TAG = KodeApp.class.getSimpleName();

    public static Context mContext;

    private Retrofit mRetrofit;
    public VkServices vkApi;
    public GoogleServices googleApi;
    public Gson gson;

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = getApplicationContext();

        mRetrofit = new Retrofit.Builder().baseUrl(KodaConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        vkApi = mRetrofit.create(VkServices.class);
        googleApi = mRetrofit.create(GoogleServices.class);
        gson = new Gson();
    }
}
