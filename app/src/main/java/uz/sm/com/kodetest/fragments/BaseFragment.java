package uz.sm.com.kodetest.fragments;

import android.support.v4.app.Fragment;
import android.view.View;

/**
 * Created by mr.boyfox on 22.01.16.
 * StyleMix Mobile LLC. All rights reserved!
 */
public class BaseFragment extends Fragment implements View.OnClickListener {
    @Override
    public void onClick(View v) {}
}
