package uz.sm.com.kodetest.helpers.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import uz.sm.com.kodetest.databinding.CountriesListItemBinding;
import uz.sm.com.kodetest.entities.SimpleItem;
import uz.sm.com.kodetest.fragments.BaseFragment;

/**
 * Created by mr.boyfox on 22.01.16.
 * StyleMix Mobile LLC. All rights reserved!
 */
public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.ViewHolder> {
    private ArrayList<SimpleItem> mData;
    private BaseFragment mFrag;
    private LayoutInflater mInflater;

    public CountriesAdapter(BaseFragment fragment, ArrayList<SimpleItem> mData) {
        this.mFrag = fragment;
        this.mInflater = LayoutInflater.from(mFrag.getActivity());
        this.mData = mData;
    }

    public void setNewData(ArrayList<SimpleItem> newData) {
        this.mData = newData;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CountriesListItemBinding clb;

        public ViewHolder(CountriesListItemBinding binder) {
            super(binder.getRoot());
            binder.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SimpleItem sItem = mData.get(getAdapterPosition());
                    v.setTag(sItem);
                    mFrag.onClick(v);
                }
            });
            clb = binder;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        CountriesListItemBinding clb = CountriesListItemBinding.inflate(mInflater, viewGroup, false);
        return new ViewHolder(clb);
    }
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        SimpleItem item = mData.get(position);
        viewHolder.clb.setSitem(item);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
