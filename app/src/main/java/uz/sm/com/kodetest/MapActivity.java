package uz.sm.com.kodetest;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uz.sm.com.kodetest.entities.City;
import uz.sm.com.kodetest.entities.SimpleItem;
import uz.sm.com.kodetest.helpers.HLog;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {
    private static final String TAG = MapActivity.class.getSimpleName();

    private KodeApp mApp;
    private GoogleMap mMap;
    private City city;
    private SimpleItem country;
    private Call<JsonObject> geoReq;
    private LatLng cityGeo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        mApp = (KodeApp) getApplication();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if (savedInstanceState != null) {
            city = savedInstanceState.getParcelable("city");
            country = savedInstanceState.getParcelable("country");
        } else {
            city = getIntent().getParcelableExtra("city");
            country = getIntent().getParcelableExtra("country");
        }
        fetchLocationByAddress();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (cityGeo != null) showCityInMap();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (geoReq != null && (!geoReq.isCanceled() || !geoReq.isExecuted())) geoReq.cancel();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("city", city);
        outState.putParcelable("country", country);
        super.onSaveInstanceState(outState);
    }

    private void fetchLocationByAddress() {
        String encodedAddress = null;
        try {
            encodedAddress = URLEncoder.encode(city.title + ", " + country.title, "utf-8");
        } catch (UnsupportedEncodingException e) { e.printStackTrace();}

        String url = String.format(Locale.US,
                "https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s",
                encodedAddress, getString(R.string.maps_api_server_key));

        HLog.i(TAG,"GEO URL : " + url);

        geoReq = mApp.googleApi.getLatLng(url);
        geoReq.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Response<JsonObject> response) {
                if (response.code() == 200) {
                    HLog.i(TAG,"GEO response : " + response);
                    JsonArray respJS = response.body().getAsJsonArray("results");
                    if (respJS.size() > 0) {
                        JsonObject geo1Js = respJS.get(0).getAsJsonObject();
                        JsonObject locJs = geo1Js.getAsJsonObject("geometry").getAsJsonObject("location");
                        cityGeo = new LatLng(locJs.getAsJsonPrimitive("lat").getAsDouble(),
                                locJs.getAsJsonPrimitive("lng").getAsDouble());
                        showCityInMap();
                    } else {
                        Toast.makeText(MapActivity.this, "Determining GEO by address fail, try again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MapActivity.this, "Determining GEO by address fail, try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                HLog.i(TAG, "Resp GEO FAIL : " + t.getLocalizedMessage());
                Toast.makeText(MapActivity.this, "Determining GEO by address fail, try again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showCityInMap() {
        HLog.i(TAG, "City location : " + cityGeo);

        MarkerOptions mopt = new MarkerOptions();
        mopt.position(cityGeo).title(String.format(Locale.US, "Marker in %s", city.title));
        if (city.important > 0) {
            mopt.snippet(String.format(Locale.US,"This is main city in %s", country.title));
        }
        mMap.addMarker(mopt);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(cityGeo));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(cityGeo, 6f));
    }
}